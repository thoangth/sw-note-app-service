package data;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import contstants.DatabaseColumns;


public class NoteDatabaseLink
{
	private static final String	    JDBC_DRIVER	= "org.h2.Driver";
	private static final String	    DB_URL	    = "jdbc:h2:~/notes";
	private static final String[]	CREDENTIALS	= new String[]
	                                            { "me", "me" };

	private static NoteDatabaseLink	_instance;

	private Connection	            conn;

	private NoteDatabaseLink()
	{
		init();
	}

	public static NoteDatabaseLink getInstance()
	{
		if ( _instance == null )
		{
			_instance = new NoteDatabaseLink();
		}

		return _instance;
	}

	public List<Map<String, Object>> getAllNotesAsList()
	{
		String query = "SELECT * FROM NOTES";
		List<Map<String, Object>> ret = new ArrayList<>();

		try
		{
			Statement stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery(query);

			while (resultSet.next())
			{
				Map<String, Object> entry = new HashMap<>();
				entry.put(DatabaseColumns.NOTE_ID, resultSet.getInt(DatabaseColumns.NOTE_ID));
				entry.put(DatabaseColumns.NOTE_NAME, resultSet.getString(DatabaseColumns.NOTE_NAME));
				entry.put(DatabaseColumns.NOTE_CONTENT, resultSet.getString(DatabaseColumns.NOTE_CONTENT));
				entry.put(DatabaseColumns.NOTE_ADDED, resultSet.getString(DatabaseColumns.NOTE_ADDED));

				ret.add(entry);
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return ret;
	}

	private void init()
	{
		try
		{
			Class.forName(JDBC_DRIVER);

			conn = DriverManager.getConnection(DB_URL, CREDENTIALS[0], CREDENTIALS[1]);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
