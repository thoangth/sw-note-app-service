package server;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import contstants.DatabaseColumns;


@Path("/notes")
public class NoteServlet extends HttpServlet
{
	/**
	 * 
	 */
	private static final long				serialVersionUID	= 1L;

	@Context
	HttpServletRequest						_request;

	@Context
	HttpServletResponse						_response;

	int										maxId				= 0;
	private static final SimpleDateFormat	sdf					= new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");

	@GET
	@Path("/all")
	@Produces("application/json")
	public Response getAllNotes()
	{
		// replace by database fetch
		String ret = buildResponseFromFile();

		return Response.ok().entity(ret).header("Access-Control-Allow-Origin", "*").build();
	}

	private String buildResponseFromFile()
	{
		URL pathToFile = NoteServlet.class.getClassLoader().getResource("/notes.json");
		String notes = "{\"notes\":[]}";
		try
		{
			FileInputStream reader = new FileInputStream(new File(pathToFile.getPath().replaceAll("%20", " ")));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			IOUtils.copy(reader, out);

			notes = new String(out.toByteArray(), "UTF-8");
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return notes;
	}

	@POST
	@Path("/add")
	public Response addNewNote(String postBody)
	{
		JsonParser parser = new JsonParser();

		JsonArray toAddArray = new JsonArray();
		JsonArray postArray = (JsonArray) parser.parse(postBody);
		for ( JsonElement element : postArray )
		{
			JsonObject newNoteObj = element.getAsJsonObject();
			newNoteObj.addProperty(DatabaseColumns.NOTE_ID, maxId++);
			newNoteObj.addProperty(DatabaseColumns.NOTE_ADDED, sdf.format(new Date()));
			newNoteObj.addProperty("note_synched", true);
			
			toAddArray.add(newNoteObj);
		}
		
		

		JsonObject allNotes = (JsonObject) parser.parse(readFromFile());
		allNotes.get("notes").getAsJsonArray().addAll(toAddArray);

		writeToFile(allNotes);

		return Response.ok().header("Access-Control-Allow-Origin", "*").build();
	}

	public String readFromFile()
	{
		URL pathToFile = this.getClass().getClassLoader().getResource("notes.json");
		String notes = "{\"notes\":[]}";
		try
		{
			FileInputStream in = new FileInputStream(new File(pathToFile.getPath().replaceAll("%20", " ")));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			IOUtils.copy(in, out);

			notes = new String(out.toByteArray(), "UTF-8");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return notes;
	}

	public boolean writeToFile(JsonObject allNotes)
	{
		FileWriter writer = null;
		URL pathToFile = this.getClass().getClassLoader().getResource("notes.json");
		try
		{
			writer = new FileWriter(new File(pathToFile.getPath().replaceAll("%20", " ")));
			writer.write(allNotes.toString());
		}
		catch (IOException e)
		{
			return false;
		}
		finally
		{
			if ( writer != null )
			{
				try
				{
					writer.close();
				}
				catch (IOException e)
				{
				}
			}
		}

		return true;
	}
}
